import './App.css'
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/RegisterPage';
import Landing from './pages/Landing';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Trips from './pages/TripsPage';
import MyBookings from './pages/MyBookings';
import { ToastContainer } from 'react-toastify';
import Seats from './pages/SeatsPage';
import ProtectedRoute from './utils/ProtectedRoutes';
import { NotFound } from './pages/NotFound';
import Nav from "./components/Nav"

function App() {
  return (
      <BrowserRouter>
        <Routes>
          <Route element={<Nav/>}>
            <Route path="/" element={<Landing />} />
            <Route path="/login" exact element={<LoginPage />} />
            <Route path="/register" exact element={<RegisterPage />} />
            <Route path="/trips" exact element={<Trips />} />
            <Route element={<ProtectedRoute/>}>
              <Route path="/preview" exact element={<Seats />} />
              <Route path="/myBookings" exact element={<MyBookings />} />
            </Route>
            <Route path='*' element={<NotFound/>}/>
          </Route>
        </Routes>
        <ToastContainer />
      </BrowserRouter>
  )
}

export default App
