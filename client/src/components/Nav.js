import { Link } from 'react-router-dom';
import Cookies from 'universal-cookie';
import React from 'react';
import { Outlet } from 'react-router-dom';

const Nav = ()=>{
    const cookies = new Cookies();
    const token = cookies.get("token");
    const logout = async()=>{
        cookies.remove("token");
        window.location.reload();
    }
    return(
        <React.Fragment>
            <nav>
                <div className="logo">
                    <Link to="/">Book<span>My</span>Bus</Link>
                </div>
                <div className="auth">
                    {token?<Link to="/" onClick={()=>logout()}>Logout</Link>:
                    <>
                    <Link to="/login">Login</Link>
                    <Link to="/register">Register</Link>
                    </>
                    }
                </div>
            </nav>
            <Outlet />
        </React.Fragment>
    )
}

export default Nav;