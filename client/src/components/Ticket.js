import axios from "../utils/axiosUtils";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";

export default function Ticket(props){
    const currDate = new Date();
    const date = currDate.getDate();
    const month = currDate.toLocaleString('default', { month: 'long' });
    const year = currDate.getFullYear();
    const navigate = useNavigate();
    function handleClick(){
        axios.post('/bus/bookABus',
        {tripId:props.id, seat:props.currBookedSeats, txnid:"txnid", src:"src", dest:"dest", date:props.date},
        {'Content-Type':'application/json'},
        ).then(response => {
            toast.success("Booked Successfully",{
                position: toast.POSITION.TOP_RIGHT,
            })
            console.log(response)
            if(response.status === 200){
                navigate('/myBookings')
            }
        }
            ).catch(err=>console.log(err))
    }
    return(
        <>
        <div className="ticket">
            <div className="logo">BookMyBus</div>
            <div className="small-text">{date} {month}, {year}</div>
            <hr className="green-hr"/>
            <div className="flex payment">
                <div>Total Amount</div>
                <div>₹ <span>{props.price*props.totalSeats}</span></div>
            </div>
            <div className="bill small-text"> 
                {props.totalSeats ? 
                <>
                    <img src={"https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=Example" + props.totalSeats} alt="Oops"/>
                    <br></br>
                    <br></br>
                    <button className="btn" onClick={()=>handleClick()}>Proceed to Payment</button>
                </>
                 :"No items to display"}
            </div>
        </div>
        </>
    )
}