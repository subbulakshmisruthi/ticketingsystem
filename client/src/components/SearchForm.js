import { useState } from "react";
import 'react-calendar/dist/Calendar.css';
import { BsArrowDownUp } from "react-icons/bs";
import busIn from "../assets/getin.png";
import busOut from "../assets/getdown.png";
import { useNavigate } from "react-router-dom";
import Calendar from 'react-calendar';
import Options from "../data/Cities";
import 'react-calendar/dist/Calendar.css';
import { toast } from 'react-toastify';

export default function SearchForm(){
    const [src, setSrc] = useState("");
    const [dest, setDest] = useState("");
    const [date, setDate] = useState(new Date());
    const navigate = useNavigate();

    async function FetchForBus(e){
        e.preventDefault();
        if(src === '' || dest === '' || src === dest){
            toast.error("Choose a valid source and a destination");
            return;
        }
        const year = date.getFullYear();
        const month = date.getMonth() + 1;
        const day = date.getDate();
        const formatted_date = month+"-"+day+"-"+year;
        navigate({
            pathname: '/trips',
            search: `?src=${src}&dest=${dest}&date=${formatted_date}`
        })
    }
    async function reversePlaces(){
        let temp = dest;
        setDest(src);
        setSrc(temp);
    }
    return(
        <>
            <div className="search-section">
                <div className="card searchform">
                    <form method="post" onSubmit={(e)=>FetchForBus(e)}>
                        <img src={busOut} alt="busin" className="icons"/>
                        <select type="text" className="inputs m-0" placeholder="Source" value={src} onChange={(ev)=>{
                            setSrc(ev.target.value); 
                        }}>
                            <option className="light-grey" selected>Source</option>
                            <Options/>
                        </select>
                        <div>
                            <BsArrowDownUp className="reverse-arrow" onClick={(ev)=>{reversePlaces()}}/>
                        </div>
                        <img src={busIn} alt="busin" className="icons"/>
                        <select type="text" className="inputs m-0" placeholder="Destination" value={dest} onChange={(ev)=>{
                            setDest(ev.target.value);
                        }} >
                            <option className="light-grey" selected>Destination</option>
                            <Options/>
                        </select>
                        <br></br>
                        <input type="submit" value={"Search bus"} className="auth-btn" />
                    </form>
                </div>
            </div>
            <div className="illustration-section">
                <div className="calendar-container">
                    <Calendar onChange={setDate} value={date} minDate={new Date()}/>
                </div>
            </div>
        </>
    )
}
