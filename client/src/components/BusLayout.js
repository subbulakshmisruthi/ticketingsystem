import { PiSteeringWheelThin } from "react-icons/pi";

export default function BusLayout(props){
    const currBookedSeats = props.currBookedSeats;
    props.setTotalSeats(currBookedSeats.length);
    function BookedSeat(props){
        return(
            <div className="seat booked-female" id={props.id}>
                <span></span>
            </div>
        )
    }
    
    function bookSeat(ev){
        if(![...currBookedSeats].includes(ev.currentTarget.id)){
            props.setCurrBookedSeats([...currBookedSeats,(ev.currentTarget.id)]);
        }
        else{
            let updated = currBookedSeats.filter(ele => (ele!==ev.currentTarget.id));
            props.setCurrBookedSeats([...updated]);
        }
    }
    function ActiveSeat(props){
        return(
            <div className={[...currBookedSeats].includes(props.id)?"seat booked":"seat"} id={props.id} onClick={ev => bookSeat(ev)} title={props.id}>
                <span></span>
            </div>
        )
    }
    return (
    <>
        <div className="bus">
            {!props.upper && <div className="steering">
                <PiSteeringWheelThin className="steering-icon" title="Driver"/>
            </div>
            }
            <hr></hr>
            {/* booked-female, booked, booked-male */}
            {[...Array((props.noOfRows)/4)].map((x, i) =>
                {return(
                    <div key={i} className="seat-section">
                        <div className="seat-left">
                            {props.bookedSeats.includes('A'+i)? <BookedSeat id={('A'+i)}/> : <ActiveSeat id={('A'+i)}/>}
                            {props.bookedSeats.includes('B'+i)? <BookedSeat id={('B'+i)}/> : <ActiveSeat id={('B'+i)}/>}
                        </div>
                        <div className="seat-right">
                            {props.bookedSeats.includes('C'+i)? <BookedSeat id={('C'+i)}/> : <ActiveSeat id={('C'+i)}/>}
                            {props.bookedSeats.includes('D'+i)? <BookedSeat id={('D'+i)}/> : <ActiveSeat id={('D'+i)}/>}
                        </div>
                    </div>
                )
                }
            )}
        </div>
    </>
    );
}
