import React from "react";
import "./error-page.css";
export const ErrorPage = (props) => {
  return (
    <div className='error-page'>
      <h1>Something went wrong</h1>
      <p>Please try again</p>
      { props.errorMessage && <p>Details - {props.errorMessage} </p>}
    </div>
  );
};