import {useState} from "react";
import { AiOutlineEye, AiOutlineEyeInvisible } from 'react-icons/ai';
import { useNavigate } from "react-router-dom";
import Cookies from 'universal-cookie';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Link } from "react-router-dom";
import axios from "../utils/axiosUtils";

export default function LoginPage(){
    const [username, setUsername]=useState('');
    const [password, setPassword]=useState('');
    const [passwordType, setPasswordType]=useState("password");
    const [disabled, setDisable]=useState('');
    const navigate = useNavigate(); 
    async function LoginUser(event){
        event.preventDefault();
        setDisable(true);
        axios.post('/user/login', {username,password},
           {'Content-Type':'application/json'},
        ).then(
            response => {
                if(response.status === 200 || response.status === 201){
                    const cookies = new Cookies();
                    cookies.set("token", response.data.token, {path:"/"});
                    toast.success("Logged in",{
                        position: toast.POSITION.TOP_RIGHT,
                    });
                    navigate('/');
                }
                else{
                    toast.error("Authentication failed",{
                        position: toast.POSITION.TOP_RIGHT,
                    });
                }
            }
            )
        .catch(err => console.log(err))
        setDisable(false);
    }
    const TogglePassword=()=>{
       if(passwordType==="password"){
        setPasswordType("text");
       }
       else{
        setPasswordType("password");
       }
    }
    return(
        <>
            <div className="loginform">
                <p className="text-center">Sign In</p>
                <form method="post" onSubmit={(e)=>LoginUser(e)}>
                    <input type="text" className="inputs" placeholder="Username" value={username} onChange={(ev)=>{
                        setUsername(ev.target.value);
                    }}/>
                    <input type={passwordType} className="inputs" placeholder="Password" value={password} onChange={(ev)=>{
                        setPassword(ev.target.value);
                    }} />
                    {passwordType==="password"? <AiOutlineEye className="eyeopen" onClick={()=>{TogglePassword()}}/> : <AiOutlineEyeInvisible className="eyeclosed" onClick={()=>{TogglePassword()}}/>} 
                    <br></br>
                    <input type="submit" value={"Login"} className="auth-btn" disabled={disabled}/>
                    <div className="ta-right">
                        <Link to="/register" className="links">New in here?</Link>
                    </div>
                </form>
            </div>
        </>
    )
}