import {useState} from "react";
import { AiOutlineEye, AiOutlineEyeInvisible } from 'react-icons/ai';
import { Link, useNavigate } from "react-router-dom";
import { toast } from 'react-toastify';
import Cookies from "universal-cookie";
import axios from "../utils/axiosUtils";

export default function RegisterPage(){
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword1] = useState('');
    const [confirmPassword, setPassword2] = useState('');
    const [passwordType1, setPasswordType1]=useState("password");
    const [disabled, setDisable]=useState('');
    const navigate = useNavigate();

    async function RegisterUser(event){
        event.preventDefault();
        setDisable(true);
        if(username.length<5){
            toast.error("Username should atleast have 5 characters",{
                position: toast.POSITION.TOP_RIGHT,
            });
            setDisable(false);
            return;
        }
        if(password!==confirmPassword){
            toast.error("Password doesn't match with confirm password",{
                position: toast.POSITION.TOP_RIGHT,
            });
            setDisable(false);
            return;
        }
        axios.post('/user/register', {username,email,password,confirmPassword},
           {'Content-Type':'application/json'},
        ).then(
            response => {
                if(response.status === 200 || response.status === 201){
                    const cookies = new Cookies();
                    cookies.set("token", response.data.token, {path:"/"});
                    toast.success("Registration successful",{
                        position: toast.POSITION.TOP_RIGHT,
                    });
                    navigate('/');
                }
                else{
                    toast.error("Registration failed",{
                        position: toast.POSITION.TOP_RIGHT,
                    });
                }
            }
            )
        .catch(err => console.log(err))
        setDisable(false);
    }
    const TogglePassword1=()=>{
       if(passwordType1==="password"){
        setPasswordType1("text");
       }
       else{
        setPasswordType1("password");
       }
    }
    const [passwordType2, setPasswordType2]=useState("password");
    const TogglePassword2=()=>{
       if(passwordType2==="password"){
        setPasswordType2("text");
       }
       else{
        setPasswordType2("password");
       }
    }
    return(
        <>
            <div className="loginform">
                <p className="text-center">Create a User</p>
                <form method="post" onSubmit={(e)=>RegisterUser(e)}>
                    <input type="text" className="inputs" placeholder="Username"  value={username}
             onChange={ev => setUsername(ev.target.value)}/>
                    <input type="email" className="inputs" placeholder="email"  value={email}
             onChange={ev => setEmail(ev.target.value)}/>
                    <input type={passwordType1} className="inputs"  placeholder="Password"  value={password}
             onChange={ev => setPassword1(ev.target.value)}/>
             {passwordType1==="password"? <AiOutlineEye className="eyeopen" onClick={()=>{TogglePassword1()}}/> : <AiOutlineEyeInvisible className="eyeclosed" onClick={()=>{TogglePassword1()}}/>} 
                    <input type={passwordType2}  className="inputs" placeholder="Confirm password"  value={confirmPassword}
             onChange={ev => setPassword2(ev.target.value)}/>
             {passwordType2==="password"? <AiOutlineEye className="eyeopen" onClick={()=>{TogglePassword2()}}/> : <AiOutlineEyeInvisible className="eyeclosed" onClick={()=>{TogglePassword2()}}/>} 
                    <br></br>
                    <input type="submit" value={"Register"} className="auth-btn" disabled={disabled}/>
                    <div className="ta-right">
                        <Link to="/login" className="links">Already have an account?</Link>
                    </div>
                </form>
            </div>
        </>
        
    )
}