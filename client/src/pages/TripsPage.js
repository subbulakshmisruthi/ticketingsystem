import { useState, useEffect } from 'react';
import { Link, useSearchParams } from 'react-router-dom';
import { AiOutlineArrowRight } from "react-icons/ai";
import { BiSolidLeftArrow,  BiSolidRightArrow} from "react-icons/bi";
import DataTable from 'react-data-table-component';
import { tableCustomStyles } from '../utils/DatatableStyles';
import Rating from '@mui/material/Rating';
import axios from '../utils/axiosUtils';

export default function Trips(){
    const [searchParams] = useSearchParams();
    const [trips, setTrips] = useState([]);
    const src = searchParams.get('src');
    const dest = searchParams.get('dest');
    const date = searchParams.get('date');
    
    useEffect(() => {
        axios.get('/bus/getBusList?' + new URLSearchParams({
            src: src,
            dest: dest,
            date: date
        }))
        .then(response=> setTrips(response.data.trips))
        .catch(err=>console.log(err))
    },[]);
    
    const columns = [
        {
            cell:(row) => <Link to={`/preview/?id=${row.id}&date=${date}`} id={row.id} className='btn'>Book</Link>,
            ignoreRowClick: true,
            allowOverflow: true,
            button: true,
        },
        {
            name: 'Buses',
            selector: row => row.bus,
        },
        {
            name: 'Departure',
            selector: row => row.departure,
        },
        {
            name: 'Duration',
            selector: row => row.duration,
        },
        {
            name: 'Arrival',
            selector: row => row.arrival,
        },
        {
            name: 'Ratings',
            selector: row => row.ratings,
        },
        {
            name: 'Fare',
            selector: row => row.fare,
        },
        {
            name: 'Seats Available',
            selector: row => row.seats,
        },
    ];
    const data = []
    for(let trip of trips){
        let dataUnit = {}
        const rating = trip.ratings;
        dataUnit["bus"] = trip.bus;
        dataUnit["id"] = trip.id;
        dataUnit["departure"] = trip.departure;
        dataUnit["duration"] = trip.duration;
        dataUnit["arrival"] = trip.arrival;
        dataUnit["fare"] = trip.fare;
        dataUnit["seats"] = trip.seats;
        dataUnit["ratings"] = <Rating name="half-rating-read" defaultValue={rating} precision={0.5} readOnly />
        data.push(dataUnit);
    }
 
    return(
        <>
            <hr></hr>
            <p className='routes'><span>{src}</span> <AiOutlineArrowRight/> <span>{dest}</span> <span><BiSolidLeftArrow/></span> {date} <span><BiSolidRightArrow/></span> <Link className='btn' to={"/"}>Change Info</Link></p>
            <hr></hr>
            <div className='data-table'>
                <DataTable
                columns={columns}
                data={data}
                pagination="true"
                customStyles={tableCustomStyles}
                />
            </div>
        </>
    )
}
