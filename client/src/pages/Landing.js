import SearchForm from "../components/SearchForm";
import 'react-calendar/dist/Calendar.css';

export default function Landing(){    
    return(
        <>
            <div className="body">
                <div className="hero-section">
                    <SearchForm/>
                </div>
            </div>
        </>
    )
}