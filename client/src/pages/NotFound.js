import { Link } from "react-router-dom"

export function NotFound(){
    return(
        <>
        <div className="not-found">
            <div>
                <h1>404</h1>
                <Link to="/" className="btn">Back to home</Link>
            </div>
        </div>
        </>
    )
}