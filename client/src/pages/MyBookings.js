import { useEffect, useState } from "react";
import axios from "../utils/axiosUtils";
import { FaBus } from "react-icons/fa";
import { toast } from "react-toastify"

export default function MyBookings(){
    const [bookings, setBookings] = useState([]);
    function cancelBooking(id){
        axios.post('/bus/cancelBooking', {
            "bookingId":id
        },{
            'Content-Type':'application/json'
        }
        ).then(response=>{
            if(response.status===204){
                let updated = bookings.filter(ele => (ele.id!==id));
                setBookings([...updated]);
                toast.success("Booking cancelled", {
                    position: toast.POSITION.TOP_RIGHT,
                })
            }
            else{
                toast.error("Something went wrong!", {
                    position: toast.POSITION.TOP_RIGHT,
                })
            }
        })
    }
    useEffect(()=>{
        axios.get('/bus/myBookings').then(response => {
            console.log(response.data)
            setBookings(response.data.bookings)
        }
            ).catch(err=>console.log(err))
    }, [])
    return(
        <>
        <div className="text-center">MyBookings</div>
        <div className="text-center j-center flex">
            {
                bookings.map((booking,i)=>{
                    return(
                    <div className="booking" key={i}>
                        <div className="flex j-space-between booking-header">
                            <div>
                                <p className="small-text">Travels</p>
                                <p>{booking.travels}</p>
                            </div>
                            <div>
                                <p className="small-text">Seat</p>
                                <p className="btn">{booking.seat}</p>
                            </div>
                        </div>
                        <div className="flex j-space-between">
                            <p className="bold">{booking.src}</p>
                            <FaBus className="bus-icon"/>
                            <p className="bold">{booking.dest}</p>
                        </div>
                        <div className="flex j-space-between circle-parent">
                            <div className="circle1"></div>
                            <div className="circle2"></div>
                        </div>
                        <hr className="grey-hr"></hr>
                        <div className="booking-timings">
                            <div className="flex j-space-between">
                                <div>
                                    <p className="small-text">Time</p>
                                    <p>{booking.startTime}</p>
                                </div>
                                <div>
                                    <p className="small-text">Date</p>
                                    <p>{booking.date}</p>
                                </div>
                            </div>
                        </div>
                        <button className="btn" onClick={()=>{cancelBooking(booking.id)}}>Cancel booking</button>
                    </div>
                    )
                })
            }          
        </div>
        </>
    )
}