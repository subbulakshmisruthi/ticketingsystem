import axios from "../utils/axiosUtils"
import { useSearchParams } from "react-router-dom";
import BusLayout from "../components/BusLayout";
import { useEffect, useState } from "react";
import Ticket from "../components/Ticket";

export default function Seats(){
    const [currBookedSeats, setCurrBookedSeats] = useState([]);
    const [searchParams] = useSearchParams();
    const [bookedSeats, setbookedSeats] = useState([]);
    const [noOfSeats, setnoOfSeats] = useState(20);
    const [price, setPrice] = useState(100);
    const [totalSeats, setTotalSeats] = useState(0);
    const id = searchParams.get("id");
    const date = searchParams.get("date");
    useEffect(()=>{
        axios.post('/bus/getBusDetails?'+ new URLSearchParams({
            id:id
        }),
        {date:date},
        {'Content-Type':'application/json'},
        ).then(response => {
            setnoOfSeats(response.data.busDoc.bus.totalSeats);
            setbookedSeats(response.data.busDoc.bookedSeats);
            setPrice(response.data.busDoc.bus.price);
            console.log({response})
        }
            ).catch(err=>console.log(err))
    }, [])
   
    return <>
        <div className="display-section">
            <div>
                <div className="seat"><span></span></div>
                <p className="small-text">Available</p>
            </div>
            <div>
                <div className="seat booked"><span></span></div>
                <p className="small-text">Selected</p>
            </div>
            <div>
                <div className="seat booked-female"><span></span></div>
                <p className="small-text">Booked</p>
            </div>
        </div>
        <div className="flex j-center">
            <BusLayout noOfRows={noOfSeats} bookedSeats={bookedSeats} currBookedSeats={currBookedSeats} setCurrBookedSeats={setCurrBookedSeats} setTotalSeats={setTotalSeats} />
            <Ticket price={price} totalSeats={totalSeats} currBookedSeats={currBookedSeats} id={id} date={date}/>
        </div>
    </>
}