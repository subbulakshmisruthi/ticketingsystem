import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from "../../utils/axiosUtils"

const initialState = {
  name:"",
  email:"",
  token:"",
  loading: "",
}

export const loginUser = createAsyncThunk(
  'user/login',
  async (payload) => {
    const response = await axios.post('/user/login', payload.data,
    {'Content-Type':'application/json'},
 )
  return response;
})

export const registerUser = createAsyncThunk(
    'user/register',
    async (payload) => {
      const response = await axios.post('/user/register', payload.data,
      {'Content-Type':'application/json'},
   )
    return response;
  })


export const userSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {
    setUser:(state, action)=>{
        state.name=action.payload.name;
        state.email=action.payload.email;
        state.token=action.payload.token;
        state.loading="idle";
    }
  },
  extraReducers: (builder)=>{
    builder.addCase(loginUser.pending, (state)=>{
        state.loading="loading";
    });
    builder.addCase(loginUser.rejected, (state)=>{
        state.loading="error";
    });
    builder.addCase(loginUser.fulfilled, (state)=>{
        state.loading="success";
    });
    builder.addCase(registerUser.pending, (state)=>{
        state.loading="loading";
    });
    builder.addCase(registerUser.rejected, (state)=>{
        state.loading="error";
    });
    builder.addCase(registerUser.fulfilled, (state)=>{
        state.loading="success";
    });
  },
})

const actions = userSlice.actions
export const {setUser} = actions
export default userSlice.reducer