import React, { useEffect, useState } from "react";
import { useNavigate, Outlet } from "react-router-dom";
import Cookies from "universal-cookie";
import axios from "../utils/axiosUtils";
import { toast } from "react-toastify"

const ProtectedRoute = (props) => {
    const navigate = useNavigate();
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const checkUserToken = () => {
        const cookies = new Cookies();
        const accessToken = cookies.get("token");
        if (!accessToken || accessToken === 'undefined') {
            setIsLoggedIn(false);
            toast.error("Login to access the page", {
                position: toast.POSITION.TOP_RIGHT
            });
            return navigate('/login');
        }
        axios.get('/user/validateToken').then(response =>{
            if(response.status !== 200){
                setIsLoggedIn(false);
                toast.error("Login to access the page", {
                    position: toast.POSITION.TOP_RIGHT
                });
                return navigate('/login');
            }
        }).catch(()=>{
            setIsLoggedIn(false);
            toast.error("Login to access the page", {
                position: toast.POSITION.TOP_RIGHT
            });
            return navigate('/login');
        }
        );
        setIsLoggedIn(true);
    }
    useEffect(() => {
            checkUserToken();
    }, [isLoggedIn]);
    return (
        <Outlet />
    );
}
export default ProtectedRoute;