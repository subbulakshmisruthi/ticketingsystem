import axios from 'axios'
import Cookies from 'universal-cookie';
axios.defaults.baseURL = 'http://localhost:4000';
const cookies = new Cookies();
const accessToken = cookies.get("token");
axios.defaults.headers.common['Authorization'] = `Bearer ${accessToken || ""}`

export default axios;