const tableCustomStyles = {
    headCells: {
      style: {
        fontSize: '14px',
        paddingLeft: '0 8px',
        justifyContent: 'center',
        marginTop: '30px',
        marginBottom: '30px'
      },
    },
    cells:{
        style:{
            textAlign: 'center',
            margin:'20px 0px'
        }
    },
  }
  export { tableCustomStyles };