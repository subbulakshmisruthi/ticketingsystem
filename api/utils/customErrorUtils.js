export class DBError extends Error {
  constructor() {
    super('DBError');
    this.name = 'DB Error';
  }
}
export class InvalidCredentialError extends Error {
  constructor() {
    super('Invalid Credentials');
    this.name = 'Invalid Credentials';
  }
}
export class UnAuthorised extends Error {
  constructor() {
    super('UnAuthorised');
    this.name = 'UnAuthorised';
  }
}
export class NotFound extends Error {
  constructor() {
    super('Not Found');
    this.name = 'Not Found';
  }
}
export class UnProcessableEntity extends Error {
  constructor(message) {
    super("Unprocessable Entity");
    this.name = message;
  }
}
export class ValidationError extends Error{
  constructor(message) {
    super("Validation error");
    this.name = message;
  }
}
export class Forbidden extends Error{
  constructor() {
    super("Validation error");
    this.name = "Forbidden";
  }
}
  