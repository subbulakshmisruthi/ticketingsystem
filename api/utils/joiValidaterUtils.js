import { ValidationError } from "./customErrorUtils.js";

export const joiValidator = async (joiObject, params)=>{
    const validator = await joiObject.validate(params);
    if(validator.error){
        throw new ValidationError(validator.error.details[0].message, 400);
    }
}