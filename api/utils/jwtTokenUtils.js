import jwt from 'jsonwebtoken';
import { UnAuthorised } from './customErrorUtils.js';

export const signJWTToken =  (payload, secret, expiryLimit)=>{
    if(expiryLimit){
        return jwt.sign(payload, secret, {expiresIn: expiryLimit});
    }
    else{
        return jwt.sign(payload, secret);
    }
}

export const verifyJWTToken = async (token, secret)=>{
    let payload;
    jwt.verify(token, secret, (err, decoded)=>{
        if(err) throw new UnAuthorised();
        payload = decoded;
    });
    return payload;
}

