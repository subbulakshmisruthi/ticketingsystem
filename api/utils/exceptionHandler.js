import {
  DBError,
  InvalidCredentialError,
  UnAuthorised,
  NotFound,
  UnProcessableEntity,
  ValidationError,
  Forbidden
} from './customErrorUtils.js';

export function asyncTryCatchMiddleware(handler) {
  return async (req, res, next) => {
    try {
      await handler(req, res);
    } catch (e) {
      console.log(e);
      if (e instanceof DBError) {
        res.status(500).json(e);
      } else if (e instanceof InvalidCredentialError) {
        res.status(400).json(e);
      } else if (e instanceof UnAuthorised) {
        res.status(401).json(e);
      } else if (e instanceof NotFound) {
        res.status(404).json(e);
      } else if (e instanceof UnProcessableEntity) {
        res.status(422).json(e);
      } else if(e instanceof ValidationError){
        res.status(400).json(e)
      } else if(e instanceof Forbidden){
        res.status(403).json(e);
      } else {
        res.status(500).json(e);
      }
    }
  };
}
