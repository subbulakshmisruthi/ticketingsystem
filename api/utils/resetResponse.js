export async function resetResponse(){
    let res = {};
    let cookie={};
    let json={};
    res.cookie = (token , value, json)=>{
      cookie[token]=value;
      return res;
    }
    res.json = (key, value)=>{
      json[key]=value;
    }
    return {res, cookie, json};
}