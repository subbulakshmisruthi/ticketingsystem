import express from 'express';
import cors from 'cors';
import mongoose from 'mongoose';
const app = express();
import cookies from 'cookie-parser';
import dotenv from 'dotenv';
dotenv.config();

import { authRouter } from './userAuth/routes/userAuth.route.js';
import { operatorRouter } from './operator/routes/operator.route.js';
import { busRouter } from './booking/routes/booking.route.js';
import { connectionHandler } from './middleware/dbConnectionHandler.js'

const corsOptions = {
  AccessControlAllowOrigin: '*',
  origin: '*',
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
};

mongoose.set('strictQuery', false);
await mongoose.connect(process.env.CONNECTION_STRING).catch((err)=>{
  console.log(err);
});

app.listen(4000);
console.log('Listening on port 4000');

app.use(cors(corsOptions));
app.use(express.json());
app.use(cookies());

app.use(connectionHandler);
app.use('/user', authRouter);
app.use('/operator', operatorRouter);
app.use('/bus', busRouter);


