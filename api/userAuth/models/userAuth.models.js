import mongoose from 'mongoose';
const {Schema, model} = mongoose;

const UserSchema = new Schema({
  username: {
    type: String, required: true, min: 4, unique: true,
  },
  password: {
    type: String, required: true,
  },
  email: {
    type: String, required: true,
  },
  created: {
    type: Date, required: true, default: Date.now(),
  },
  role: {
    type: String, enum: ['User', 'Operator', 'Driver'], default: 'User',
  },
});

export const UserModel = model('User', UserSchema);