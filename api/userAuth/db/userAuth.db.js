import {UserModel} from '../models/userAuth.models.js';
import { DBError } from '../../utils/customErrorUtils.js';

export async function createUser(username, password, email) {
    const userDoc = await UserModel.create({
        username,
        password,
        email
    }).catch(()=>{throw new DBError();});

    return userDoc;
}

export function findUser(username) {
    const userDoc = UserModel.findOne({username})
    .catch(()=>{throw new DBError();});
    return userDoc;
}
