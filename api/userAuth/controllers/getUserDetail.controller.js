import { Forbidden } from "../../utils/customErrorUtils.js"
import { findUser } from "../db/userAuth.db.js";

export const userDetails = async(req, res)=>{
    if(req.user){
        const {username} = req.user;
        const user = await findUser(username);
        res.status(200).json({"user":user});
    }
    else{
        throw new Forbidden();
    }
}