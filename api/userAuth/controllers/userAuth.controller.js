import { loginService } from '../services/login.service.js';
import { registerService } from '../services/register.service.js';
import { refreshService } from '../services/refresh.service.js';
import { UnAuthorised } from '../../utils/customErrorUtils.js';

export const register = async (req, res) => {
  const {username, email, password, confirmPassword} = req.body;
  const {accessToken, refreshToken} = await registerService(
    username, 
    email, 
    password, 
    confirmPassword
    );
  res.cookie('rftoken', refreshToken, {
    httpOnly: true,
    sameSite: 'None',
    secure: true,
    maxAge: 24 * 60 * 60 * 1000,
  }).json({'token': accessToken});
};

export const login = async (req, res) => {
  const {username, password} = req.body;
  const {accessToken, refreshToken} = await loginService(username, password);

  res.cookie('rftoken', refreshToken, {
    httpOnly: true,
    sameSite: 'None',
    secure: true,
    maxAge: 24 * 60 * 60 * 1000,
  }).json({'token': accessToken});

};

export const refresh = async (req, res) => {
  if (req.cookies?.rftoken) {
    const token = await refreshService(req.cookies.rftoken);
    res.json({'token': token});
  } 
  else {
    throw new UnAuthorised();
  }

};

export const logout = async(req, res)=>{
  if (req.cookies?.token) {
    res.clearCookie("token");
    res.status(200).json({"status":"Logged out"});
    res.end();
  } 
  else {
    throw new UnAuthorised();
  }
}

export const validateToken = async(req, res)=>{
  res.status(200).json({"status":"OK"})
}