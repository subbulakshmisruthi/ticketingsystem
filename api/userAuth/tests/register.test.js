import mongoose from "mongoose";
import { register } from "../controllers/userAuth.controller.js";
import { ValidationError } from "../../utils/customErrorUtils.js";

let res = {};
let cookie={};
let json={};

beforeEach(async () => {
    await resetResponse();
    const connection = await mongoose.connect(process.env.TEST_CONNECTION_STRING)
    .catch(err=>console.log(err));
});

describe("Authentication tests - Register", ()=>{

    it("Register valid user", async ()=>{
        await register({"body":
          {username:"testuser", email:"testuser@gmail.com", password:"testUser@123", confirmPassword:"testUser@123"}
        }, res);
        expect(cookie).not.toBeNull();
        expect(cookie).toHaveProperty("token");
    }),

    it("Register Invalid User - email", async()=>{
      let error;
      await register({"body":
          {username:"testuserFail1", email:"testuser", password:"testUser@123", confirmPassword:"testUser@123"}
        }, res).catch((err)=>{error=err;});
      expect(cookie).toEqual({});
      expect(error instanceof ValidationError).toBeTruthy();
      expect(error.name).toBe("Email should contain a @ followed by a valid domain");
    }),

    it("Register Invalid User - Password Constraint", async()=>{
      let error;
      await register({"body":
          {username:"testuserFail2", email:"testuser", password:"testUser", confirmPassword:"testUser"}
        }, res).catch((err)=>{error=err;});
      expect(cookie).toEqual({});
      expect(error instanceof ValidationError).toBeTruthy();
      expect(error.name).toBe("Password should contain a alphabet, a number and a digit");
    })

})

afterEach(async () => {
  await mongoose.connection.db.dropDatabase();
  await mongoose.connection.close();
});

async function resetResponse(){
  res = {};
  cookie={};
  json={};
  res.cookie = (token , value, json)=>{
    cookie[token]=value;
    return res;
  }
  res.json = (payload)=>{
    json=payload;
  }
}