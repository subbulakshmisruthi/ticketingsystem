import mongoose from "mongoose";
import { refresh, register } from "../controllers/userAuth.controller.js";
import { UnAuthorised } from "../../utils/customErrorUtils.js";

let res = {};
let cookie={};
let json={};

beforeEach(async () => {
    await resetResponse();
    const connection = await mongoose.connect(process.env.TEST_CONNECTION_STRING)
    .catch(err=>console.log(err));
});

describe("Authentication tests - Refresh", ()=>{

    it("Refresh token for a valid user", async ()=>{
        await register({"body":
          {username:"testuserrefresh", email:"testuser@gmail.com", password:"testUser@123", confirmPassword:"testUser@123"}
        }, res);
        await refresh({"cookies":cookie}, res);
        expect(json).not.toBeNull();
        expect(json).toHaveProperty("token");
    }),

    it("Refresh token for Invalid User", async()=>{
      let error;
      await refresh({}, res).catch((err)=>{error=err;});
      expect(json).toEqual({});
      expect(error instanceof UnAuthorised).toBeTruthy();
    })

})

afterEach(async () => {
    await mongoose.connection.db.dropDatabase();
    await mongoose.connection.close();
});

async function resetResponse(){
  res = {};
  cookie={};
  json={};
  res.cookie = (token , value, json)=>{
    cookie[token]=value;
    return res;
  }
  res.json = (payload)=>{
    json=payload;
  }
}
