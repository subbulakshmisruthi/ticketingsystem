import mongoose from "mongoose";
import { login, register } from "../controllers/userAuth.controller.js";
import { InvalidCredentialError } from "../../utils/customErrorUtils.js";

let res = {};
let cookie={};
let json={};

beforeEach(async () => {
    await resetResponse();
    const connection = await mongoose.connect(process.env.TEST_CONNECTION_STRING).catch(err=>{
      console.log("DB connection error", err);
    });
});

describe("Authentication tests - Login", ()=>{

    it("Login valid user", async ()=>{
        await register({"body":
          {username:"testuserlogin", email:"testuser@gmail.com", password:"testUser@123", confirmPassword:"testUser@123"}
        }, res);
        await resetResponse();
        await login({"body":
        {
          username:"testuserlogin",password:"testUser@123",
        }}, res);
        expect(cookie).not.toBeNull();
        expect(cookie).toHaveProperty("token");
    }),

    it("Login Invalid User - username incorrect", async()=>{
      let error;
      await login({"body":
        {
          username:"userWhoDoesntExist",password:"testUser@123",
        }}, res).catch((err)=>{error=err;});
        expect(cookie).toEqual({});
        expect(error instanceof InvalidCredentialError).toBeTruthy();
    }),

    it("Login Invalid User - password incorrect", async()=>{
      await register({"body":
          {username:"testuserlogin", email:"testuser@gmail.com", password:"testUser@123", confirmPassword:"testUser@123"}
        }, res);
      await resetResponse();
      let error;
      await login({"body":
        {
          username:"testuserlogin",password:"testUser@1234",
        }}, res).catch((err)=>{error=err;});
        expect(cookie).toEqual({});
        expect(error instanceof InvalidCredentialError).toBeTruthy();
    })
})

afterEach(async () => {
    await mongoose.connection.db.dropDatabase();
    await mongoose.connection.close();
});

async function resetResponse(){
  res = {};
  cookie={};
  json={};
  res.cookie = (token , value, json)=>{
    cookie[token]=value;
    return res;
  }
  res.json = (key, value)=>{
    json[key]=value;
  }
}
