import {Router} from 'express';
import {register, login, refresh, logout} from '../controllers/userAuth.controller.js';

import {asyncTryCatchMiddleware} from '../../utils/exceptionHandler.js';
import { authenticateUser } from '../../middleware/authmiddleware.js';
import { userDetails } from '../controllers/getUserDetail.controller.js';
import { validateToken } from '../controllers/userAuth.controller.js';

export const authRouter = new Router();

authRouter.post('/register', asyncTryCatchMiddleware(register));
authRouter.post('/login', asyncTryCatchMiddleware(login));
authRouter.post('/refresh', asyncTryCatchMiddleware(refresh));
authRouter.use(authenticateUser);
authRouter.get('/validateToken', asyncTryCatchMiddleware(validateToken))
authRouter.get('/userDetails',asyncTryCatchMiddleware(userDetails));
authRouter.post('/logout',asyncTryCatchMiddleware(logout));
