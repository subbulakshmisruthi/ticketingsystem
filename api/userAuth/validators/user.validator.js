import Joi from 'joi';
import { PASSWORD_REGEX } from '../../utils/constants.js';

export const UserValidator = Joi.object({

    username: Joi.string().min(5).max(100).required().messages({
      'string.base': 'Username should be of type text',
      'string.min': 'Username should atleast have 5 characters',
      'string.max': 'Username shoudld not have any more than 100 characters',
      'any.required': 'Username is a required field',
    }),

    password: Joi.string().min(8).max(300).regex(
        new RegExp(PASSWORD_REGEX),
    ).required().messages({
      'string.base': 'Password should be of type text',
      'string.min': 'Password should be length of more than 8',
      'string.max': 'Password can only ever contain atmost 300 characters',
      'any.required': 'Password is a required field',
      'string.pattern.base':
      'Password should contain a alphabet, a number and a digit',
    }),

    confirmPassword: Joi.any().
        equal(Joi.ref('password')).required().messages(
            {'any.only': 'Password and Confirm Password does not match'},
        ),

    email: Joi.string().email().required().messages({
      'string.base': 'Email should be of type text',
      'string.email': 'Email should contain a @ followed by a valid domain',
      'any.required': 'Email is a required field',
    }),

});
  