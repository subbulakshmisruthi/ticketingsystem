import bcrypt from 'bcryptjs';
import { findUser } from '../db/userAuth.db.js';
import { InvalidCredentialError } from '../../utils/customErrorUtils.js';
import { signJWTToken } from '../../utils/jwtTokenUtils.js';

export const loginService = async (username, password)=>{
    const user = await findUser(username);
    if (!user) {
      throw new InvalidCredentialError();
    }

    const passOk = bcrypt.compareSync(password, user.password);
    if (passOk) {
      const userInfo = {username, id: user._id};
      const accessToken = signJWTToken(userInfo, process.env.ACCESS_SECRET, '10m')
      const refreshToken = signJWTToken(userInfo, process.env.REFRESH_SECRET, '1d');
      return {accessToken, refreshToken};
    }

    throw new InvalidCredentialError();
};