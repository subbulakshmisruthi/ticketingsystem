import { signJWTToken, verifyJWTToken } from '../../utils/jwtTokenUtils.js';

export const refreshService = async (refreshToken)=>{
    const decoded  = await verifyJWTToken(refreshToken, process.env.REFRESH_SECRET);
    return signJWTToken(decoded, process.env.ACCESS_SECRET);
};
  