import bcrypt from 'bcryptjs';
import { UserValidator } from '../validators/user.validator.js';
import { createUser } from '../db/userAuth.db.js';
import { signJWTToken } from '../../utils/jwtTokenUtils.js';
import { joiValidator } from '../../utils/joiValidaterUtils.js';


export const registerService = async (username, email, password, confirmPassword)=>{
  await joiValidator(UserValidator, 
    {username, password, email, confirmPassword}
  )
    
  const salt = bcrypt.genSaltSync(10);
  const userDoc = await createUser(
    username,
    bcrypt.hashSync(password, salt),
    email,
  );

  const user = {username, id: userDoc._id};
  const accessToken = signJWTToken(user, process.env.ACCESS_SECRET, '10m');
  const refreshToken = signJWTToken(user, process.env.REFRESH_SECRET, '1d');

  return {accessToken, refreshToken};
};