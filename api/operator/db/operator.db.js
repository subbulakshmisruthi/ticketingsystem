import { DBError } from '../../utils/customErrorUtils.js';
import {Operator} from '../models/operator.models.js';
import {Trip} from '../../booking/models/trip.models.js';
import {UserModel} from '../../userAuth/models/userAuth.models.js';
import mongoose from 'mongoose';

export const createOperator = async(json)=>{
    const opDoc = await Operator.create(json).catch(()=>{throw new DBError();});
    return opDoc;
}

export const deleteOperatorById = async(id)=>{
    const opDoc = await Operator.deleteOne({_id: new mongoose.Types.ObjectId(id)}).catch(()=>{throw new DBError();});
    return opDoc;
}

export const findOperatorById = async(id)=>{
    const opDoc = await Operator.findOne({_id:new mongoose.Types.ObjectId(id)}).catch(()=>{throw new DBError();});
    return opDoc;
}

export const createTrip = async(json)=>{
    const opDoc = await Trip.create(json).catch(()=>{throw new DBError();});
    return opDoc;
}

export const updateUserRole = async(id, role)=>{
    const userDoc = await UserModel.findOneAndUpdate(
        {_id: new mongoose.Types.ObjectId(id)},
        {role: role}).catch(()=>{throw new DBError();});
    return userDoc;
}


