import { DBError } from '../../utils/customErrorUtils.js';
import {Bus} from '../../booking/models/bus.models.js';
import mongoose from 'mongoose';

export const createBus = async(json)=>{
    const busDoc = await Bus.create(json).catch(()=>{throw new DBError();});
    return busDoc;
}

export const deleteBusById = async(id)=>{
    const busDoc = await Bus.deleteOne({_id: new mongoose.Types.ObjectId(id)}).catch(()=>{throw new DBError();});
    return busDoc;
}

export const updateBusById = async(id, json)=>{
    const busDoc = await Bus.findOneAndUpdate(
        {_id: new mongoose.Types.ObjectId(id)},
        json).catch(()=>{throw new DBError();});
    return busDoc;
}