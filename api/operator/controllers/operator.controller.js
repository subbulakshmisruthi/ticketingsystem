import {
  createOperatorService,
  createTripService,
} from '../services/operator.service.js';
import {
  deleteOperatorById,
  updateUserRole,
} from '../db/operator.db.js';

export const createOperator = async (req, res)=>{
  const {opname, address, phone, userId} = req.body;
  await createOperatorService(opname, address, phone, userId);
  res.status(201).json({'Status': 'Created'});
};

export const deleteOperator = async (req, res)=>{
  await deleteOperatorById(req.body.operatorId);
  res.status(204);
};

export const createTrip = async (req, res)=>{
  const { routes, operator, startTime, endTime, totalSeats, price, availableDays } = req.body;
  await createTripService(routes, operator, startTime, endTime, totalSeats, price, availableDays);
  res.status(201).json({'Status': 'Created'});
};

export const grantAccess = async (req, res)=>{
  const {userId} = req.body;
  await updateUserRole(userId, 'Operator');
  res.status(201).json({'Status': 'Modified'});
};
