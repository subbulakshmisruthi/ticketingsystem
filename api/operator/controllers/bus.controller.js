import { 
    createBusService, 
    editBusService,
  } from '../services/operator.service.js';
import { deleteBusById } from '../db/bus.db.js';

export const createBus = async (req, res)=>{
    const {busName, Ac, operator, noOfSeats, price} = req.body;
    await createBusService(busName, Ac, operator, noOfSeats, price);
    res.status(201).json({'Status': 'Created'});
};

export const deleteBus = async (req, res)=>{
    await deleteBusById(req.body.busId);
    res.status(204);
};

export const editBus = async (req, res)=>{
    const busId = req.query.busId;
    const {busName, Ac, operator, noOfSeats, busLayout} = req.body;
    await editBusService(busId, busName, Ac, operator, noOfSeats, busLayout);
    res.status(201).json({'Status': 'Modified'});
};