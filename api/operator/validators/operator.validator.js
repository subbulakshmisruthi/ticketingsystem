import Joi from "joi";

export const operatorValidator = Joi.object({
    user: Joi.string().hex().length(24).messages(
      {
        'string.hex':'Provided operator is invalid',
        'string.length':'Provided operator is invalid',
        'any.invalid': 'Provided user is invalid'
      },
    ),
    opname: Joi.string().required().messages({
      'string.base': 'Operator Name should be of type text',
      'any.required': 'Operator Name is a required field',
    }),
    address: Joi.string().min(10).max(1000).required().messages({
      'string.base': 'Address Should be a text',
      'string.min': 'Address should be of length greater than 10',
      'string.max': 'Address should be of length lesser than 1000',
      'any.required': 'Address is a required field',
    }),
    phone: Joi.number().min(1111111111).max(9999999999).required().messages({
      'number.base': 'Phone number should be a valid number',
      'number.min': 'Phone number should exactly have 10 digits',
      'number.max': 'Phone number should exactly have 10 digits',
      'any.required': 'Phone number is a required field',
    }),
  });
  