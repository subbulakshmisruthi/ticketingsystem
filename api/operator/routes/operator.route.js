import {Router} from 'express';
import {
  createOperator,
  deleteOperator,
  createTrip,
  grantAccess
} from '../controllers/operator.controller.js';
import {
  createBus,
  deleteBus,
  editBus,
} from '../controllers/bus.controller.js'
import { authenticateUser } from '../../middleware/authmiddleware.js';
import { asyncTryCatchMiddleware } from '../../utils/exceptionHandler.js';
import { isOperator } from '../../middleware/operatorRoleMiddleware.js';

export const operatorRouter = new Router();

operatorRouter.use(authenticateUser);
operatorRouter.use(isOperator);
operatorRouter.post('/createBus', asyncTryCatchMiddleware(createBus));
operatorRouter.post('/deleteBus', asyncTryCatchMiddleware(deleteBus));
operatorRouter.post('/editBus', asyncTryCatchMiddleware(editBus));
operatorRouter.post('/createOperator', asyncTryCatchMiddleware(createOperator));
operatorRouter.post('/deleteOperator', asyncTryCatchMiddleware(deleteOperator));
operatorRouter.post('/createTrip', asyncTryCatchMiddleware(createTrip));
operatorRouter.post('/grantAccess', asyncTryCatchMiddleware(grantAccess));
