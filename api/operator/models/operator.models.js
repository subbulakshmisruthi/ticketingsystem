import mongoose from 'mongoose';
const {Schema, model} = mongoose;

const operatorSchema = new Schema({
  user: {
    type: mongoose.Types.ObjectId, required: true,
  },
  opname: {
    type: String, required: true, min: 4, unique: true,
  },
  address: {
    type: String, required: true,
  },
  phone: {
    type: String, required: true,
  },
});

export const Operator = model('Operator', operatorSchema);