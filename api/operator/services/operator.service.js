import { busValidator } from '../../booking/validators/bus.validator.js';
import { operatorValidator } from '../validators/operator.validator.js';
import { tripValidator } from '../../booking/validators/trip.validator.js';
import mongoose from 'mongoose';
import {
  createOperator,
  createTrip,
} from '../db/operator.db.js';
import {
  createBus,
  updateBusById,
} from '../db/bus.db.js'
import { joiValidator } from '../../utils/joiValidaterUtils.js';

export const createBusService = async (busName, Ac, operator, noOfSeats, price)=>{
  await joiValidator(busValidator,
      {busName, price, operator, noOfSeats},
  );

  const busDoc = await createBus({
    busname: busName,
    operator: new mongoose.Types.ObjectId(operator),
    isAc: Ac,
    noOfSeats: noOfSeats,
    price: price,
  });
  return busDoc;
};

export const editBusService = async (busId, busName, Ac, operator, noOfSeats, busLayout)=>{
  await joiValidator(busValidator,
    {busName, price, operator, noOfSeats},
  );

  const busDoc = await updateBusById(
      busId, {
        busname: busName,
        operator: new mongoose.Types.ObjectId(operator),
        isAc: Ac,
        noOfSeats: noOfSeats,
        busLayout: busLayout,
      });
  return busDoc;
};

export const createOperatorService = async (opname, address, phone, userId)=>{
  await joiValidator(operatorValidator,
      {opname: opname, address: address, phone: phone, user: userId},
  );

  const opDoc = await createOperator({
    opname: opname,
    address: address,
    phone: phone,
    user: new mongoose.Types.ObjectId(userId)
  });
  return opDoc;
};

export const createTripService = async (routes, operator, startTime, endTime, totalSeats, price, availableDays)=>{
  console.log(routes, operator, startTime, endTime, totalSeats, price);
  await joiValidator(tripValidator,{
    startTime: startTime,
    endTime: endTime,
    travelAgency: operator,
    totalSeats: totalSeats,
    price: price,
  });

  const tripDoc = await createTrip({
    routes: routes,
    travelAgency: new mongoose.Types.ObjectId(operator),
    startTime: startTime,
    endTime: endTime,
    totalSeats: totalSeats,
    availableSeats: totalSeats,
    price: price,
    availableDays: availableDays
  });
  return tripDoc;
};

