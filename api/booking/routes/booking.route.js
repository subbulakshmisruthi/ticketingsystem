import {Router} from 'express';
import {
  bookABus,
  myBookings,
  cancelBooking,
} from '../controllers/booking.controller.js';
import { getBusList, getBusDetails } from '../controllers/busDetail.controller.js';
import {authenticateUser} from '../../middleware/authmiddleware.js';
import {asyncTryCatchMiddleware} from '../../utils/exceptionHandler.js';

export const busRouter = new Router();

busRouter.use(authenticateUser);
busRouter.get('/getBusList', asyncTryCatchMiddleware(getBusList));
busRouter.post('/getBusDetails', asyncTryCatchMiddleware(getBusDetails));
busRouter.post('/bookABus', asyncTryCatchMiddleware(bookABus));
busRouter.get('/myBookings', asyncTryCatchMiddleware(myBookings));
busRouter.post('/cancelBooking', asyncTryCatchMiddleware(cancelBooking));
