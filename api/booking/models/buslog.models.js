import mongoose from 'mongoose';
const {Schema, model} = mongoose;

const buslogSchema = new Schema({
  date: {
    type: String, required: true,
  },
  bus: {
    type: mongoose.Types.ObjectId, required: true,
  },
  trip: {
    type: mongoose.Types.ObjectId, required: true,
  },
});

export const Buslog = model('BusLog', buslogSchema);
