import mongoose from 'mongoose';
const {Schema, model} = mongoose;

const bookingSchema = new Schema({
  user: {
    type: mongoose.Types.ObjectId, required: true,
  },
  trip: {
    type: mongoose.Types.ObjectId, required: true,
  },
  date: {
    type: Date,
  },
  bookedAt: {
    type: Date,
  },
  txnid: {
    type: String, required: true,
  },
  seat: {
    type: Array,
  },
  src: {
    type: String, required: true,
  },
  dest: {
    type: String, required: true,
  },
  status: {
    type: String,
    enum: ['On hold', 'Upcoming', 'Cancelled', 'Past'],
    default: 'Upcoming',
  }
});

export const Booking = model('Booking', bookingSchema);
