import mongoose from 'mongoose';
const {Schema, model} = mongoose;
import Joi from 'joi';

const tripSchema = new Schema({
  travelAgency: {
    type: mongoose.Types.ObjectId, required: true,
  },
  routes: {
    type: Array, required: true,
  },
  startTime: {
    type: String,
  },
  endTime: {
    type: String,
  },
  totalSeats: {
    type: Number, required: true,
  },
  availableSeats: {
    type: Number,
  },
  isActive: {
    type: Boolean, default: true,
  },
  price: {
    type: Number,
  },
  availableDays: {
    type: Array, required: true, default: [0, 1, 2, 3, 4, 5, 6],
  },
  ratings: {
    type: Number, default: 5
  }
});

export const Trip = model('Trip', tripSchema);