import mongoose from 'mongoose';
const {Schema, model} = mongoose;

const BusSchema = new Schema({
  busname: {
    type: String, required: true, min: 4, unique: true,
  },
  operator: {
    type: mongoose.Types.ObjectId, required: true,
  },
  isAc: {
    type: Boolean,
  },
  noOfSeats: {
    type: Number, required: true,
  },
  price: {
    type: Number, required: true,
  },
});

export const Bus = model('Bus', BusSchema);