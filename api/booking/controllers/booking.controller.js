import { bookBusService } from '../services/booking.service.js';
import { myBookingService } from '../services/booking.service.js';
import {
  findBooking, 
  updateBookingById
} from '../db/booking.db.js';

export const bookABus = async (req, res)=>{
  const { tripId, seat, txnid, src, dest, date } = req.body;
  const bookDoc = await bookBusService(tripId, seat, txnid, src, dest, date, req.user.id);
  res.status(201).json({'bookDoc': bookDoc});
};

export const myBookings = async (req, res)=>{
  const bookings = await myBookingService(req.user.id);
  res.status(200).json({'bookings': bookings});
};

export const cancelBooking = async (req, res)=>{
  const {bookingId} = req.body;
  await updateBookingById(
      bookingId, {status: 'Cancelled'},
  );
  res.status(204);
};
