import { 
    getBusListService, 
    getBusDetailService
} from '../services/busDetail.service.js';

export const getBusList = async (req, res)=>{
    const {src, dest} = req.query;
    const trips = await getBusListService(src, dest);
    res.status(200).json({'trips': trips});
};
  
  export const getBusDetails = async (req, res)=>{
    const { date } = req.body;
    const busId = req.query.id;
    const busDoc = await getBusDetailService(date, busId);
    res.status(200).json({'busDoc': busDoc});
};