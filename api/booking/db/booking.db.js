import { DBError } from '../../utils/customErrorUtils.js';
import {Booking} from '../models/booking.models.js';
import {Trip} from '../models/trip.models.js';
import mongoose from 'mongoose';
import {Operator} from '../../operator/models/operator.models.js'
import { calculateDuration } from '../utils/calculateDuration.js';

export const findTrip = (async(src, dest)=>{
    const tripDoc = await Trip.find({'routes': {'$all':[src, dest]}}).catch(()=>{throw new DBError();});
    let trips = []
    for(let doc of tripDoc){
        let trip = {}
        trip['id'] = doc._id;
        const operator = await Operator.findOne({_id:new mongoose.Types.ObjectId(doc.travelAgency)}).catch(()=>{throw new DBError();});
        trip['bus'] = operator.opname;
        trip['departure'] = doc.startTime;
        trip['duration'] = calculateDuration(doc.startTime, doc.endTime)
        trip['arrival'] = doc.endTime;
        trip['ratings'] = doc.ratings;
        trip['fare'] = doc.price;
        trip['seats'] = doc.availableSeats;
        trip['routes'] = doc.routes;
        trips.push(trip);
    }
    return trips;
});

export const findTripById = async(tripId)=>{
    const tripDoc = await Trip.findOne({_id:new mongoose.Types.ObjectId(tripId)}).catch(()=>{throw new DBError();});
    return tripDoc;
}

export const findBooking = async(json)=>{
    const bookingDoc = await Booking.find(json).catch((err)=>{console.log(err);throw new DBError();});
    return bookingDoc;
}

export const createBooking = async(json)=>{
    const bookingDoc = await Booking.create(json).catch(()=>{throw new DBError();});
    return bookingDoc;
}

export const updateBookingById = async(id, json)=>{
    const bookDoc = await Booking.findOneAndUpdate(
        {_id: new mongoose.Types.ObjectId(id)},
        json).catch(()=>{throw new DBError();});
    return bookDoc;
}
