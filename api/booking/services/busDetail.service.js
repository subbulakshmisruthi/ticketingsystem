import { NotFound } from '../../utils/customErrorUtils.js'
import { findBooking, findTrip, findTripById } from '../db/booking.db.js';
import { tripsInOrder, getBookedSeats } from '../utils/busDetailUtils.js';
import mongoose from 'mongoose';

export const getBusListService = async (src, dest)=>{
  const trips = await findTrip(src, dest);
  const tripList = await tripsInOrder(trips, src, dest);
  return tripList;
};

export const getBusDetailService = async (date, id)=>{
    const bus = await findTripById(id);
    if(!bus) throw new NotFound();

    const bookings = await findBooking({
    trip: new mongoose.Types.ObjectId(bus._id),
    date: new Date(date), status: {'$ne': 'Cancelled'}},
    );

    const busDoc = {};
    busDoc['bookedSeats'] = await getBookedSeats(bookings);
    busDoc['bus'] = bus;
    return busDoc;
};