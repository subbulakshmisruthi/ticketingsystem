import { findTripById, createBooking, findBooking } from '../db/booking.db.js';
import { checkIfBookingExists, checkIfTripValid } from '../utils/bookingUtils.js';
import { findOperatorById } from '../../operator/db/operator.db.js';
import mongoose from 'mongoose';

export const bookBusService = async (tripId, seat, txnid, src, dest, date, userId)=>{

  const trip = await findTripById(tripId);

  await checkIfTripValid(trip);
  await checkIfBookingExists(tripId, date, seat);
  
  const bookDoc = await createBooking({
    user: new mongoose.Types.ObjectId(userId),
    trip: new mongoose.Types.ObjectId(tripId),
    src: src,
    dest: dest,
    seat: seat,
    txnid: txnid,
    date: new Date(date),
  });
  return bookDoc;
};

export const myBookingService = async (id)=>{
  const bookingDoc = await findBooking(
    {
      user: new mongoose.Types.ObjectId(id),
      status: {"$ne":"Cancelled"},
    },
  );
  let bookings = []
  for(let doc of bookingDoc){
    let booking = {};
    let trip = await findTripById(doc.trip);
    booking["travels"] = await findOperatorById(trip.travelAgency).opname;
    booking["src"]=doc.src;
    booking["dest"]=doc.dest;
    booking["startTime"]=trip.startTime;
    booking["date"]=doc.bookedAt;
    booking["id"]=doc._id;
    bookings.push(booking);
  }
  return bookings;
}