import Joi from "joi";
import { isValid } from "../utils/validatorUtils.js";

export const busValidator = Joi.object({
    busName: Joi.string().required().min(10).messages(
        {
          'string.base': 'Bus name should be value text',
          'string.min': 'Bus name should have a minimum length of 10',
          'any.required': 'Bus name is a required field',
        },
    ),
    operator: Joi.string().hex().length(24).required().messages(
      {
        'string.hex':'Provided operator is invalid',
        'string.length':'Provided operator is invalid',
        'any.invalid':'Provided operator is invalid'
      },
    ),
    noOfSeats: Joi.number().min(10).max(50).required().custom(isValid).messages(
        {
          'number.base': 'Number of Seats must be an integer',
          'number.min': 'Number of seats should be greater 10',
          'number.max': 'Number of seats should be less than 50',
          'number.required': 'Number of seats is a required field',
          'any.invalid': 'Number of seats should be in order of four',
        },
    ),
    price: Joi.number().min(200).max(10000).required().messages({
      'number.base': 'Price must be an integer',
      'number.min': 'Price should be greater than 200',
      'number.max': 'Price should be less than 10000',
      'number.required': 'Price is a required field',
    }),
});