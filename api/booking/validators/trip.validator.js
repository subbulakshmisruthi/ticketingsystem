import Joi from "joi";
import { isValid } from "../utils/validatorUtils.js";
import { TIME_REGEX } from "../../utils/constants.js";

export const tripValidator = Joi.object({
    startTime: Joi.string().required().regex(
        new RegExp(TIME_REGEX),
    ).messages({
      'string.base': 'Start time should be a valid time',
      'any.required': 'Start time is a required field',
      'string.pattern.base': 'Start time should be a valid time',
    }),
    travelAgency: Joi.string().hex().required().length(24).messages(
      {
        'string.hex':'Provided operator is invalid',
        'string.length':'Provided operator is invalid',
        'any.invalid': 'Provided Travel Agency is invalid',
        'any.required':'Operator is required'
      },
    ),
    endTime: Joi.string().required().regex(
        new RegExp(TIME_REGEX),
    ).messages({
      'string.base': 'End time should be a valid time',
      'any.required': 'End time is a required field',
      'string.pattern.base': 'End time should be a valid time',
    }),
    totalSeats: Joi.number().min(10).max(50).required().custom(isValid).messages({
      'number.base': 'Total number of seats must be an integer',
      'number.min': 'Number of seats should be greater 10',
      'number.max': 'Number of seats should be less than 50',
      'number.required': 'Number of seats is a required field',
      'any.invalid': 'Number of seats should be in order of four',
    }),
    price: Joi.number().min(200).max(10000).required().messages({
      'number.base': 'Price must be an integer',
      'number.min': 'Price should be greater than 200',
      'number.max': 'Price should be less than 10000',
      'number.required': 'Price is a required field',
    }),
});
  