import { UnProcessableEntity } from '../../utils/customErrorUtils.js'
import { findBooking } from '../db/booking.db.js';
import mongoose from 'mongoose';

export async function checkIfTripValid(trip){
    if (!trip.isActive) {
      throw new UnProcessableEntity("Trip currently not active");
    }
    if (trip.availableSeats < 1) {
      throw new UnProcessableEntity("Chosen Seat is already filled");
    }
}

export async function checkIfBookingExists(tripId, date, seat){
    const bookings = await findBooking({
        trip: new mongoose.Types.ObjectId(tripId),
        date: new Date(date),
        seat: {"$in": seat},
        status: {'$ne': 'Cancelled'},
    });
    if (bookings.length) {
        throw new UnProcessableEntity("Chosen Seat is already filled");
    }
}
  