function toSeconds(time_str) {
    var parts = time_str.split(':');
    return parts[0] * 3600 + 
           parts[1] * 60 +   
           +parts[2];        
}

export function calculateDuration(a, b){
    var difference = Math.abs(toSeconds(a) - toSeconds(b));
    var result = [
        Math.floor(difference / 3600), 
        Math.floor((difference % 3600) / 60), 
        difference % 60 
    ];
    
    result = result.map(function(v) {
        return v < 10 ? '0' + v : v;
    }).join(':');
    return result;
}