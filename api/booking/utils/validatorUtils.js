export const isValid = (value, helpers)=>{
    if (typeof value !== 'number') return helpers.error('any.invalid');
    if (value % 4 == 0) {
      return value;
    } else {
      return helpers.error('any.invalid');
    }
};