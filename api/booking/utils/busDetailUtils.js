export async function tripsInOrder(trips, src, dest){
    const tripList=[];
    trips.forEach((element)=>{
      let i; let j;
      for (let index=0; index<element.routes.length; index++) {
        if (element.routes[index]==src) {
          i = index;
        } else if (element.routes[index]==dest) {
          j = index;
        }
      }
      if (i<j) {
        delete element['routes'];
        tripList.push(element);
      }
    });
    return tripList;
}

export async function getBookedSeats(bookings){
    let seats = [];
    for (const booking of bookings){
        seats = [...seats, ...booking.seat]
    }
    return seats;
}