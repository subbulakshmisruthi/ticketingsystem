import { UnAuthorised } from '../utils/customErrorUtils.js';
import { UserModel } from '../userAuth/models/userAuth.models.js';

export const isOperator = async (req, res, next) => {
  if (req?.user) {
    const userDoc = await UserModel.findOne({_id: req.user.id});
    if (userDoc && userDoc.role == 'Operator') {
      next();
    } else {
      throw new UnAuthorised();
    }
  } else {
    throw new UnAuthorised();
  }
};