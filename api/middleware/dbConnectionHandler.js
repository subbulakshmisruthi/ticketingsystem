import mongoose from "mongoose";

export async function connectionHandler(req, res, next) {
    if (mongoose.connection.readyState ==! 1) {
      await mongoose.connect(process.env.CONNECTION_STRING).catch((err)=>{
        res.status(500).json({"status":"DB connection failed"});
        return;
      });
    }
    next();
};