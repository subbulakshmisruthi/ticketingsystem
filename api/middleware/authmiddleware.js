import jwt from 'jsonwebtoken';
import { UnAuthorised } from '../utils/customErrorUtils.js';

export function authenticateUser(req, res, next) {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];
  if (token == null) throw new UnAuthorised();
  jwt.verify(token, process.env.ACCESS_SECRET, (err, user) => {
    if (err) {
      return res.sendStatus(403)
    };
    req.user = user;
    next();
  });
}
